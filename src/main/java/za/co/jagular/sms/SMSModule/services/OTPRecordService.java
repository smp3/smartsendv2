package za.co.jagular.sms.SMSModule.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import za.co.jagular.sms.SMSModule.entities.OTPRecord;
import za.co.jagular.sms.SMSModule.repository.OTPRepositoryImpl;
import za.co.jagular.sms.SMSModule.transferObjects.OTPRecordDTO;
import za.co.jagular.sms.Utilities.RestLogger;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Random;

@Service
public class OTPRecordService {
    @Autowired
    private OTPRepositoryImpl otpDAO;
    Logger logger = LoggerFactory.getLogger(OTPRecordService.class);

    public ResponseEntity<?> verifyIncomingOTP(String otp, String destination, String application){
        logger.info("Verifying OTP For: "+destination);
        try {
            OTPRecord otpRecord = otpDAO.getOTP(destination, application);
            if (otpRecord == null){
                logger.info("No OTP Record found for: "+destination);

                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Could not find any OTP for: "+destination+" and application: "+application);
            }else {
                if (otpRecord.getOtp().equalsIgnoreCase( otp)){
                    logger.info("Verification Success For: "+destination);

                    return ResponseEntity.status(HttpStatus.OK).body(RestLogger.toJSON("Verification Success"));
                }else {
                    logger.info("Unable to verify For: "+destination);

                    return ResponseEntity.status(HttpStatus.FORBIDDEN).body(RestLogger.toJSON("Verification failure"));
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(RestLogger.toJSON("Something went wrong"));
        }
    }

    public String addNewOTPRecord(OTPRecord otpRecord) {
        logger.info("Checking if incoming OTP record exists");
        try {
            OTPRecord otp = otpDAO.getOTP(otpRecord.getDestination(), otpRecord.getApplication());
            if (otp == null) {
                logger.info("New OTP Record for number: " + otpRecord.getDestination());
                if (otpDAO.saveOTP(otpRecord)) {
                    logger.info("Saved New OTP Record: ", otpRecord);
                    return "true";
                } else {
                    logger.error("Unable to save OTP Record");
                    return "false";
                }
            } else {
                //If record exists, update with new otp and expiry
                otp.setOtp(otpRecord.getOtp());
                otp.setExpiry(15);
                if(otpDAO.saveOTP(otp)){
                    logger.info("Updated OTP For: "+otp.getDestination());
                    return "true";
                }else {
                    logger.error("Unable to update OTP For: "+otp.getDestination());
                    return "false";
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            return "error";
        }
    }

}
