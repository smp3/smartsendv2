package za.co.jagular.sms.SMSModule.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import za.co.jagular.sms.SMSModule.entities.OTPRecord;

@Component
public class OTPRepositoryImpl {
    @Autowired
    private OTPRecordRepository recordRepository;

    public Boolean saveOTP(OTPRecord otpRecord){
        try {
            recordRepository.save(otpRecord);
            System.out.printf("OTP Saved");
            return true;
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("Unable to Save OTP");
            return false;
        }

    }
    public OTPRecord getOTP(String destination, String application){
        try {
          return  recordRepository.findByApplicationAndDestination(application, destination);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }
}
