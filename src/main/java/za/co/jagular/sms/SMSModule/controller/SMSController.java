package za.co.jagular.sms.SMSModule.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import za.co.jagular.sms.SMSModule.services.OTPRecordService;
import za.co.jagular.sms.SMSModule.services.SMSServices;
import za.co.jagular.sms.SMSModule.transferObjects.Messages;
import za.co.jagular.sms.SMSModule.transferObjects.OTPRecordDTO;

import javax.annotation.security.PermitAll;

@RestController
@CrossOrigin
public class SMSController {

    @Autowired
    private SMSServices smsServices;

    @PostMapping("/send/otp/{destination}/{application}")
    public ResponseEntity<?> sendOutOTP(@PathVariable String destination, @PathVariable String application ){
        return smsServices.sendOTP(destination, application);

    }
    @PostMapping("/send/otp")
    public ResponseEntity<?> sendOutOTP(@RequestBody Messages message){
        return smsServices.sendOTP(message);

    }

}
