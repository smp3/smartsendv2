package za.co.jagular.sms.SMSModule.entities;

import za.co.jagular.sms.Utilities.AuditModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity

public class ProcessedSMS extends AuditModel {
    @Id
    private String id;
    private String sourceApplication;
    private String smsDelivered;
    private String destination;
    @Column(columnDefinition = "TEXT")
    private String dispatchedMessage;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSourceApplication() {
        return sourceApplication;
    }

    public void setSourceApplication(String sourceApplication) {
        this.sourceApplication = sourceApplication;
    }

    public String getSmsDelivered() {
        return smsDelivered;
    }

    public void setSmsDelivered(String smsDelivered) {
        this.smsDelivered = smsDelivered;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDispatchedMessage() {
        return dispatchedMessage;
    }

    public void setDispatchedMessage(String dispatchedMessage) {
        this.dispatchedMessage = dispatchedMessage;
    }
}
