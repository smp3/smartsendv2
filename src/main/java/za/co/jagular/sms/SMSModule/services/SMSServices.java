package za.co.jagular.sms.SMSModule.services;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import za.co.jagular.sms.SMSModule.entities.OTPRecord;
import za.co.jagular.sms.SMSModule.transferObjects.Messages;

import java.time.LocalDateTime;

@Service
public class SMSServices {

    @Autowired
    private AuthorizationService authorizationService;
    @Autowired
    private OTPRecordService otpRecordService;
    private Logger logger = LoggerFactory.getLogger(SMSServices.class);

//    private char[] generateOTP() {
//        logger.info("Generating OTP");
//        String numbers = "1234567890";
//        Random random = new Random();
//        int length = 2;
//        char[] otp = new char[length];
//        for (int i = 0; i < length; i++) {
//            otp[i] = numbers.charAt(random.nextInt(numbers.length()));
//        }
//        logger.info("OTP Generated : "+otp);
//
//        return otp;
//    }
    public  String generateOTP() {
        logger.info("Generating OTP");
        int randomPin   =(int)(Math.random()*9000)+1000;
        String otp  =String.valueOf(randomPin);
        logger.info("OTP Generated : "+otp);
        return otp;
    }

    private LocalDateTime generateOTPexpiry(int minutes) {
        logger.info("Generating OTP Expiry");
        LocalDateTime dateTime = LocalDateTime.now();
        return dateTime.plusMinutes(minutes);
    }

    public ResponseEntity<?> sendOTP(String destination, String application){
        logger.info("Sending out OTP to: "+destination);
        try {
            Messages otpMessage = new Messages();
            String otp = generateOTP();
            otpMessage.setMessage("Hi there. Your OTP is: "+otp+" #Jagular SmartSend");
            otpMessage.setMobileNumber(destination);
            otpMessage.setApplication(application);
            String authorizationToken = authorizationService.getAuthorizationToken();
           return sendMessage(authorizationToken, otpMessage, otp);
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something went wrong");
        }

    }

    public ResponseEntity<?> sendOTP(Messages otpBody){
        logger.info("Sending out OTP to: "+otpBody);
        try {
            String authorizationToken = authorizationService.getAuthorizationToken();
            return sendMessage(authorizationToken, otpBody, "otp");
        }catch (Exception e){
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something went wrong");
        }

    }

    private ResponseEntity<?> sendMessage(String token, Messages otpMessage, String otp) {
        OkHttpClient client = new com.squareup.okhttp.OkHttpClient();
        RequestBody sendRequestBody = RequestBody.create(
                com.squareup.okhttp.MediaType.parse("application/json; charset=utf-8"),
                "{\n" +
                        "  \"messages\": [\n" +
                        "    {\n" +
                        "      \"content\": \"" + otpMessage.getMessage() + "\",\n" +
                        "      \"destination\": \"" + otpMessage.getMobileNumber() + "\"\n" +
                        "    }\n" +
                        "  ]\n" +
                        "}");

        Request sendRequest = new com.squareup.okhttp.Request.Builder()
                .url("https://rest.smsportal.com/v1/bulkmessages")
                .header("Authorization", "Bearer " + token)
                .post(sendRequestBody)
                .build();
        try {
            Response sendResponse = client.newCall(sendRequest).execute();
            System.out.println(sendResponse.body().string());
            //Upon successful SMS sending, audit the record
            OTPRecord smsRecord = new OTPRecord();
            smsRecord.setExpiry(generateOTPexpiry(15));
            smsRecord.setApplication(otpMessage.getApplication());
            smsRecord.setDestination(otpMessage.getMobileNumber());
            smsRecord.setOtp(otpMessage.getMessage());
            String newOTPRecord = otpRecordService.addNewOTPRecord(smsRecord);
            if (newOTPRecord == "true"){
                return ResponseEntity.status(HttpStatus.OK).body(smsRecord);
            }else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Unable to save OTP Record");
            }

        } catch (
                Exception e) {
            System.out.println("Request failed. Exception: " + e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("something went wrong");
        }

    }

}
