package za.co.jagular.sms.SMSModule.transferObjects;

import java.util.ArrayList;
import java.util.List;

public class SMSRequestDTO {

    private String requestingApplicaiton;
    private List<Messages> messages = new ArrayList<>();

    public String getRequestingApplicaiton() {
        return requestingApplicaiton;
    }

    public void setRequestingApplicaiton(String requestingApplicaiton) {
        this.requestingApplicaiton = requestingApplicaiton;
    }

    public List<Messages> getMessages() {
        return messages;
    }

    public void setMessages(List<Messages> messages) {
        this.messages = messages;
    }
}
