package za.co.jagular.sms.SMSModule.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import za.co.jagular.sms.SMSModule.services.OTPRecordService;

@RestController
@CrossOrigin
public class OTPVerificationRecordController {
    @Autowired
    private OTPRecordService otpRecordService;

    @PostMapping("/verify/otp/{otp}/{destination}/{application}")
    public ResponseEntity<?> verifyOTP(@PathVariable String otp, @PathVariable String destination, @PathVariable String application){
        return otpRecordService.verifyIncomingOTP(otp, destination,application);

    }
}
