package za.co.jagular.sms.SMSModule.services;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import za.co.jagular.sms.SMSModule.transferObjects.SMSRequestDTO;
import za.co.jagular.sms.Utilities.RestLogger;

import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

//Class that calls the sms portal api to get a new access key
//This is first done by concatenating the "clientID:APIkey" and then converting it to a base64 string
@Service
public class AuthorizationService {

    @Autowired
    private Environment environment;

    private   String readSMSCredentials (){
        String clientID = environment.getProperty("sms.id");
        String secret = environment.getProperty("sms.secret");
        return clientID+":"+secret;
    }
    private String credentialsToBase64(){
      return Base64.getEncoder().encodeToString(readSMSCredentials().getBytes());
    }
    public String getAuthorizationToken() {
        RestAssured.baseURI = "https://rest.smsportal.com/v1/";
        io.restassured.response.Response response = null;
        Map<String, String> headers = new HashMap<>();
        try {
            headers.put("Authorization", "Basic " + credentialsToBase64());
            response = given().contentType(ContentType.JSON)
                    .headers(headers)
                    .get("Authentication");
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Response :" + response.asString());
        String token = response.jsonPath().getString("token");
        return  token;
    }

}
