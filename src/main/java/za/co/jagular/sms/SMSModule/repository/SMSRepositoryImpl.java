package za.co.jagular.sms.SMSModule.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import za.co.jagular.sms.SMSModule.entities.ProcessedSMS;

@Component
public class SMSRepositoryImpl {
    @Autowired
    private SMSRepository smsRepository;

    public boolean saveSMS(ProcessedSMS processedSMS){
        try {
            smsRepository.save(processedSMS);
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }
}
