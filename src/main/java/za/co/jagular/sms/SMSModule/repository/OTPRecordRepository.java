package za.co.jagular.sms.SMSModule.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import za.co.jagular.sms.SMSModule.entities.OTPRecord;

public interface OTPRecordRepository extends JpaRepository<OTPRecord, String> {
    OTPRecord findByApplicationAndDestination(String application, String destination);

}
