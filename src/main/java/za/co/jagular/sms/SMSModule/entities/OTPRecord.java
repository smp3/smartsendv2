package za.co.jagular.sms.SMSModule.entities;

import za.co.jagular.sms.Utilities.AuditModel;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.UUID;

@Entity
public class OTPRecord extends AuditModel {
    @Id
    private String id;
    private String destination;
    private String otp;
    private String application;

    private LocalDateTime expiry;

    public OTPRecord(){
        this.id = UUID.randomUUID().toString();
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public LocalDateTime getExpiry() {
        return expiry;
    }

    public void setExpiry(LocalDateTime expiry) {
        this.expiry = expiry;
    }
    public void setExpiry(int minutes){
        Calendar now = Calendar.getInstance();
        now.add(Calendar.MINUTE, minutes);
        LocalDateTime expiryDate = now.getTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

}
