package za.co.jagular.sms.SMSModule.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import za.co.jagular.sms.SMSModule.entities.ProcessedSMS;

public interface SMSRepository extends JpaRepository<ProcessedSMS, String> {
}
