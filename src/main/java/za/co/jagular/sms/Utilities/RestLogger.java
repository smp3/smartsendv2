package za.co.jagular.sms.Utilities;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.swing.text.DateFormatter;
import java.time.LocalDateTime;
import java.util.Optional;

public class RestLogger extends DateFormatter {

    public static ObjectMapper mapper = new ObjectMapper();
    public static String jsonInString;

    public static String toJSON(Object object){
        try{
            if(object instanceof Optional){

                jsonInString = mapper.writeValueAsString(((Optional) object).get()).replace(",",",\n\n");
            }else {
                jsonInString = mapper.writeValueAsString(object).replace(",",",\n");

            }
        }catch (Exception e){
            System.out.println("failed to convert to string");
            e.printStackTrace();
        }
        return jsonInString;
    }

    public static void log(String message, Object jsonObj){
        System.out.println(LocalDateTime.now()+" : "+message+" : "+jsonObj+"\n");
    }
}
