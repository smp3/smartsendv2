package za.co.jagular.sms;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit4.SpringRunner;
import za.co.jagular.sms.SMSModule.transferObjects.Messages;
import za.co.jagular.sms.SMSModule.transferObjects.SMSRequestDTO;
import za.co.jagular.sms.Utilities.RestLogger;

import java.io.IOException;
import java.util.*;

import static io.restassured.RestAssured.given;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SmsApplicationTests implements CommandLineRunner {
    @Autowired
    Environment environment;
    private SMSRequestDTO smsRequestDTO;
    private Messages messagePayload1;
    private Messages messagePayload2;
    private Messages messagePayload3;
    private List<Messages> messages;

    @Test
    public void contextLoads() {
    }

    @Before
    public void setUp() {
        String content = "Welcome to the Future, Engineered by Jagular";
        messages = new ArrayList<>();
        smsRequestDTO = new SMSRequestDTO();
        messagePayload1 = new Messages();
        messagePayload2 = new Messages();
        messagePayload3 = new Messages();

        messagePayload1.setMessage(content);
        messagePayload1.setMobileNumber("0658152376");
        messagePayload2.setMessage(content);
        messagePayload2.setMobileNumber("0714505666");
        messagePayload3.setMessage(content);
        messagePayload3.setMobileNumber("0815535228");

        messages.add(messagePayload1);
        messages.add(messagePayload2);
        messages.add(messagePayload3);

        smsRequestDTO.setRequestingApplicaiton("CHEF TEST");
        smsRequestDTO.setMessages(messages);

    }


    public void testSMS(String token) {
        OkHttpClient client = new com.squareup.okhttp.OkHttpClient();
        RequestBody sendRequestBody = RequestBody.create(
                com.squareup.okhttp.MediaType.parse("application/json; charset=utf-8"),
                "{\n" +
                        "  \"messages\": [\n" +
                        "    {\n" +
                        "      \"content\": \"" + "Hello SMS World" + "\",\n" +
                        "      \"destination\": \"" + "0658152376" + "\"\n" +
                        "    }\n" +
                        "  ]\n" +
                        "}");

        Request sendRequest = new com.squareup.okhttp.Request.Builder()
                .url("https://rest.smsportal.com/v1/bulkmessages")
                .header("Authorization", "Bearer " + token)
                .post(sendRequestBody)
                .build();
        try {
            Response sendResponse = client.newCall(sendRequest).execute();
            System.out.println(sendResponse.body().string());
        } catch (
                IOException e) {
            System.out.println("Request failed. Exception: " + e.getMessage());
        }
    }

    public String readSMSCredentials() {
        String clientID = environment.getProperty("sms.id");
        String secret = environment.getProperty("sms.secret");
        return clientID + ":" + secret;
    }

    public String credentialsToBase64() {
        return Base64.getEncoder().encodeToString(readSMSCredentials().getBytes());
    }

    public String smsRequestDTOtoJSON(List<Messages> messages) {
        String messagesAsJSON = RestLogger.toJSON(messages);
        System.out.println("SMS Request: " + messagesAsJSON);
        return messagesAsJSON;
    }

    public String smsRequestDTOtoJSON(SMSRequestDTO smsRequest) {
        String messagesAsJSON = RestLogger.toJSON(smsRequest);
        System.out.println("SMS Request: " + messagesAsJSON);
        return messagesAsJSON;
    }

@Ignore
    @Test
    public void getAuthorization() {
        RestAssured.baseURI = "https://rest.smsportal.com/v1/";
        io.restassured.response.Response response = null;
        Map<String, String> headers = new HashMap<>();
        try {
            headers.put("Authorization", "Basic " + credentialsToBase64());
            response = given().contentType(ContentType.JSON)
                    .headers(headers)
                    .get("Authentication");
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Response :" + response.asString());
        String token = response.jsonPath().getString("token");
        System.out.println("Token :" + token);
        ///testSMS(token);
        //estDynamicSMS(token);


    }
@Ignore
    @Test
    public void testGetAuthorizationToken() {
        OkHttpClient client = new com.squareup.okhttp.OkHttpClient();
        RequestBody sendRequestBody = RequestBody.create(
                com.squareup.okhttp.MediaType.parse("application/json; charset=utf-8"),
                smsRequestDTOtoJSON(smsRequestDTO)
        );

        Request sendRequest = new com.squareup.okhttp.Request.Builder()
                .url("https://rest.smsportal.com/v1/Authentication")
                .header("Authorization", "Basic " + credentialsToBase64())
                .get()
                .build();
        try {
            Response sendResponse = client.newCall(sendRequest).execute();
            System.out.println(sendResponse.body().string());
        } catch (
                IOException e) {
            System.out.println("Request failed. Exception: " + e.getMessage());
        }
    }


    public void testDynamicSMS(String token) {
        OkHttpClient client = new com.squareup.okhttp.OkHttpClient();
        RequestBody sendRequestBody = RequestBody.create(
                com.squareup.okhttp.MediaType.parse("application/json; charset=utf-8"),
                smsRequestDTOtoJSON(smsRequestDTO)
        );

        Request sendRequest = new com.squareup.okhttp.Request.Builder()
                .url("https://rest.smsportal.com/v1/bulkmessages")
                .header("Authorization", "Bearer " + token)
                .post(sendRequestBody)
                .build();
        try {
            Response sendResponse = client.newCall(sendRequest).execute();
            System.out.println(sendResponse.body().string());
        } catch (
                IOException e) {
            System.out.println("Request failed. Exception: " + e.getMessage());
        }
    }

    @Override
    public void run(String... args) throws Exception {

    }
}
